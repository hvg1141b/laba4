/* 3. Создать метод, который будет сортировать указанный массив по возрастанию любым
 * известным вам способом.
 */

/**
 *
 * @author Виктор
 */
public class Task3 {
//сортировка методом пузырька
    public static void sort(int[] a) {
        for (int i = a.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (a[j] > a[j + 1]) {
                    int c = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = c;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] b;
        b = new int[20];
        //инициализация и вывод
        for (int i = 0; i < 20; i++) {
            b[i] = (int) (Math.random() * 101);
            System.out.print(b[i] + " ");
        }
        System.out.println();
        //вызов метода
        sort(b);
        //вывод отсортированого массива
        for (int i = 0; i < 20; i++) {
            System.out.print(b[i] + " ");
        }
    }
}